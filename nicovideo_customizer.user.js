// ==UserScript==
// @name         Nicovideo customizer
// @namespace    https://javelin.works
// @version      0.1.3
// @description  ニコニコ動画のページをカスタマイズ
// @author       sanadan <jecy00@gmail.com>
// @match        https://www.nicovideo.jp/*
// ==/UserScript==

(function () {
  'use strict'

  document.head.insertAdjacentHTML('beforeend', `
<style>
:root {
  --border-color: green;
}
a.TimelineItem-contentBody {
  border: 1px solid lightgray;
  border-radius: 8px;
}
a:visited.TimelineItem-contentBody {
  border-color: var(--border-color);
}
a:visited .TimelineItem-contentTitle,
section.bdr_m a:visited {
  color: var(--border-color);
}
a:visited .NC-CardTitle {
  color: var(--border-color) !important;
}

.common-header-nvc {
  display: flex;
  align-items: center;
  height: 100%;
}
.common-header-nvc .btn {
  padding: 0 1rem;
  color: white;
  border: 1px solid white;
  border-radius: 5px;
  cursor: pointer;
}
.common-header-nvc .spacer {
  width: 0.5rem;
}
</style>
`)

  const NVC = (function () {
    let observer
    let focusFlag
    let addFlag
    return {
      init: async function () {
        // 動画ページだけ対応
        if (!location.href.startsWith('https://www.nicovideo.jp/watch/')) return

        focusFlag = false
        addFlag = false
        observer = new MutationObserver(records => {
          records.forEach(record => {
            record.addedNodes.forEach(node => {
              this.detect(node)
            })
          })
        })
        observer.observe(document.body, { childList: true, subtree: true })
      },
      detect: function(node) {
        if (!focusFlag) {
          focusFlag = this.focusTextArea()
        }

        if (!addFlag) {
          const base = document.querySelector('#CommonHeader > div > div > div > div')
          if (base !== null) {
            addFlag = true
            base.insertAdjacentHTML('afterend', `
<div class="common-header-nvc">
  <div class="play btn">動画再生</div>
  <div class="spacer"></div>
  <div class="info btn">詳細表示</div>
</div>
`)
            document.querySelector('.play.btn').addEventListener('click', function () {
              const video = document.querySelector('video')
              window.scrollTo(0, 0)
              video.play()
              NVC.focusTextArea()
            })
            document.querySelector('.info.btn').addEventListener('click', function () {
              const player = document.getElementsByClassName('grid-area_[player]')[0]
              const styles = window.getComputedStyle(player)
              const content = document.querySelector('div[aria-label="nicovideo-content"]')
              const paddingTop = parseFloat(window.getComputedStyle(content).paddingTop)
              window.scrollTo(0, player.offsetHeight + paddingTop)
              document.querySelector('video').pause()
            })
          }
        }

        if (focusFlag && addFlag) {
          observer.disconnect()
        }
      },
      focusTextArea: function() {
        const textarea = document.querySelector('textarea[placeholder="コメントを入力"]')
        console.log(textarea)
        if (textarea !== null) {
          textarea.focus()
          return true
        }

        return false
      }
    }
  })()

  NVC.init()
})()
